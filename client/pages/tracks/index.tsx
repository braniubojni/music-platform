import { Box } from "@mui/material";
import React from "react";
import MainLayout from "../../layouts/MainLayout";

function Index() {
  return (
    <MainLayout>
      <Box component="h1">Hello from tracks</Box>
    </MainLayout>
  );
}

export default Index;
